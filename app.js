/**
 * Created by baranbaygan on 21/11/2016.
 */

const awsIot = require('aws-iot-device-sdk');
//const gpio = new require('./lib/gpio')();
const SensorAdapter = require('./lib/sensors-adapter');

//
// Replace the values of '<YourUniqueClientIdentifier>' and '<YourAWSRegion>'
// with a unique client identifier and the AWS region you created your
// certificate in (e.g. 'us-east-1').  NOTE: client identifiers must be
// unique within your AWS account; if a client attempts to connect with a
// client identifier which is already in use, the existing connection will
// be terminated.
//
var thingShadows = awsIot.thingShadow({
    keyPath: "cert/private.pem.key",
    certPath: "cert/certificate.pem.crt",
    caPath: "cert/root-CA.crt",
    clientId: "AliKamilAlarmCenterClient",
    region: "eu-west-1"
});

//
// Client token value returned from thingShadows.update() operation
//
var clientTokenUpdate;

//
// Simulated device values
//
var rval = 188;
var gval = 114;
var bval = 222;

var sensors = {
    "mainDoor": {
        "sensorType": "digital",
        "pin": 12,
        "alarmValue": false,
        "canBypass": true
    },
    "lightBarrierLow": {
        "sensorType": "digital",
        "pin": 13,
        "alarmValue": true,
        "canBypass": true
    },
    "lightBarrierLowReceiverTamper": {
        "sensorType": "digital",
        "pin": 14,
        "alarmValue": false,
        "canBypass": false
    },
    "lightBarrierLowTransmitterTamper": {
        "sensorType": "digital",
        "pin": 15,
        "alarmValue": false,
        "canBypass": false
    },
    "mainSirenTamper": {
        "sensorType": "digital",
        "pin": 16,
        "alarmValue": false,
        "canBypass": false
    }
}


var sensors = new SensorAdapter();

sensors.on('test', function(){
   console.log("tested");
});

sensors.doSomething();


/*
thingShadows.on('connect', function () {


    console.log("connected");


    thingShadows.register('AliKamilAlarmCenter', {}, function () {


        var rgbLedLampState = {
            "state": {
                "reported": {
                    "alarmRunning": false,
                    "red": rval,
                    "green": gval,
                    "blue": bval
                }
            }
        };

        //clientTokenUpdate = thingShadows.get('AliKamilAlarmCenter');
        clientTokenUpdate = thingShadows.update('AliKamilAlarmCenter', rgbLedLampState);

        if (clientTokenUpdate === null) {
            console.log('update shadow failed, operation still in progress');
        }
    });

    thingShadows.on('status',
        function (thingName, stat, clientToken, stateObject) {
            console.log('received ' + stat + ' on ' + thingName + ': ' +
                JSON.stringify(stateObject));
        });

    thingShadows.on('delta',
        function (thingName, stateObject) {
            console.log('received delta on ' + thingName + ': ' +
                JSON.stringify(stateObject));
        });

    thingShadows.on('timeout',
        function (thingName, clientToken) {
            console.log('received timeout on ' + thingName +
                ' with token: ' + clientToken);
        });

});
*/