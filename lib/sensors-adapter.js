/**
 * Created by baranbaygan on 22/11/2016.
 */

const Gpio = require('./gpio');
const events = require('events');
const inherits = require('util').inherits;

var gpio = new Gpio();

function Sensors(sensorSettings) {

    var self = this;

    if (!(this instanceof Sensors)) {
        return new Sensors(sensorSettings);
    }






    events.EventEmitter.call(this);

    return self;
}

inherits(Sensors, events.EventEmitter);

module.exports = Sensors;

